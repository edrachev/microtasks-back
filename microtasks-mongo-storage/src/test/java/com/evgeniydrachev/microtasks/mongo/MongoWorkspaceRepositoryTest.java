package com.evgeniydrachev.microtasks.mongo;

import com.evgeniydrachev.microtasks.core.workspace.Category;
import com.evgeniydrachev.microtasks.core.workspace.Task;
import com.evgeniydrachev.microtasks.core.workspace.Workspace;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class MongoWorkspaceRepositoryTest {

    @Test
    public void testWriteRead() {
        try (EmbeddedMongo mongo = new EmbeddedMongo()) {
            MongoClient mongoClient = MongoClients.create(format("mongodb://localhost:%s", mongo.getPort()));
            MongoWorkspaceRepository repository = new MongoWorkspaceRepository(mongoClient);

            UUID userId = UUID.randomUUID();
            Workspace workspace = repository.getOrCreateDefault(userId);

            workspace.addTask(new Task("today task - 2"), Category.TODAY);
            workspace.addTask(new Task("today task - 1"), Category.TODAY);

            workspace.addTask(new Task("soon task - 2"), Category.SOON);
            workspace.addTask(new Task("soon task - 1"), Category.SOON);

            workspace.addTask(new Task("later task - 2"), Category.LATER);
            workspace.addTask(new Task("later task - 1"), Category.LATER);

            repository.save(workspace);

            Workspace rereadWorkspace = repository.get(workspace.getId()).get();

            List<Task> todayTasks = rereadWorkspace.getTasks(Category.TODAY);
            assertEquals(2, todayTasks.size());
            assertEquals("today task - 1", todayTasks.get(0).getSummary());
            assertEquals("today task - 2", todayTasks.get(1).getSummary());
            assertNotNull(todayTasks.get(0).getCreated());
            assertNotNull(todayTasks.get(1).getCreated());

            List<Task> soonTasks = rereadWorkspace.getTasks(Category.SOON);
            assertEquals(2, soonTasks.size());
            assertEquals("soon task - 1", soonTasks.get(0).getSummary());
            assertEquals("soon task - 2", soonTasks.get(1).getSummary());
            assertNotNull(soonTasks.get(0).getCreated());
            assertNotNull(soonTasks.get(1).getCreated());

            List<Task> tasks = rereadWorkspace.getTasks(Category.LATER);
            assertEquals(2, tasks.size());
            assertEquals("later task - 1", tasks.get(0).getSummary());
            assertEquals("later task - 2", tasks.get(1).getSummary());
            assertNotNull(tasks.get(0).getCreated());
            assertNotNull(tasks.get(1).getCreated());
        }
    }

    @Test
    public void testCompleted() {
        try (EmbeddedMongo mongo = new EmbeddedMongo()) {
            MongoClient mongoClient = MongoClients.create(format("mongodb://localhost:%s", mongo.getPort()));
            MongoWorkspaceRepository repository = new MongoWorkspaceRepository(mongoClient);

            UUID userId = UUID.randomUUID();
            Workspace workspace = new Workspace(userId);

            Instant task1Date = Instant.now().truncatedTo(ChronoUnit.MILLIS);

            workspace.addTask(new Task(UUID.randomUUID(), "today task - 2", false, task1Date), Category.TODAY);
            Task taskOne = new Task("today task - 1");
            workspace.addTask(taskOne, Category.TODAY);

            workspace.completeTask(taskOne.getUuid());

            repository.save(workspace);

            Workspace rereadWorkspace = repository.get(workspace.getId()).get();

            List<Task> todayTasks = rereadWorkspace.getTasks(Category.TODAY);
            assertEquals(1, todayTasks.size());
            assertEquals("today task - 2", todayTasks.get(0).getSummary());

            List<Task> completedTasks = rereadWorkspace.getCompletedTasks(Category.TODAY);
            assertEquals(1, completedTasks.size());
            assertEquals("today task - 1", completedTasks.get(0).getSummary());
            assertEquals(task1Date, completedTasks.get(0).getCreated());
        }
    }

    @Test
    public void testDefaultWorkspaceName() {
        try (EmbeddedMongo mongo = new EmbeddedMongo()) {
            MongoClient mongoClient = MongoClients.create(format("mongodb://localhost:%s", mongo.getPort()));
            MongoWorkspaceRepository repository = new MongoWorkspaceRepository(mongoClient);

            UUID userId = UUID.randomUUID();
            Workspace workspace = repository.getOrCreateDefault(userId);

            assertEquals("Tasks", workspace.getName());
        }
    }

    @Test
    public void testDefaultWorkspaceNameByMigration() {
        try (EmbeddedMongo mongo = new EmbeddedMongo()) {
            MongoClient mongoClient = MongoClients.create(format("mongodb://localhost:%s", mongo.getPort()));

            MongoDatabase database = mongoClient.getDatabase("microtasks");
            MongoCollection<Document> collection = database.getCollection("workspaces");

            UUID userId = UUID.randomUUID();

            Document document = new Document("_id", UUID.randomUUID().toString())
                    .append("userId", userId.toString())
                    .append("TODAY", Collections.emptyList())
                    .append("SOON", Collections.emptyList())
                    .append("LATER", Collections.emptyList());
            collection.insertOne(document);

            MongoWorkspaceRepository repository = new MongoWorkspaceRepository(mongoClient);

            List<Workspace> workspaces = repository.getAll(userId);

            assertEquals(1, workspaces.size());
            assertEquals("Tasks", workspaces.get(0).getName());
        }
    }

    @Test
    public void createSeveralWorkspaces() {
        try (EmbeddedMongo mongo = new EmbeddedMongo()) {
            MongoClient mongoClient = MongoClients.create(format("mongodb://localhost:%s", mongo.getPort()));
            MongoWorkspaceRepository repository = new MongoWorkspaceRepository(mongoClient);

            UUID userId = UUID.randomUUID();
            repository.save(new Workspace("workspace-1", userId));
            repository.save(new Workspace("workspace-2", userId));
            repository.save(new Workspace("workspace-3", userId));

            List<Workspace> workspaces = repository.getAll(userId);

            assertEquals(3, workspaces.size());
            assertEquals("workspace-1", workspaces.get(0).getName());
            assertEquals("workspace-2", workspaces.get(1).getName());
            assertEquals("workspace-3", workspaces.get(2).getName());
        }
    }

    @Test
    public void removeWorkspace() {
        try (EmbeddedMongo mongo = new EmbeddedMongo()) {
            MongoClient mongoClient = MongoClients.create(format("mongodb://localhost:%s", mongo.getPort()));
            MongoWorkspaceRepository repository = new MongoWorkspaceRepository(mongoClient);

            UUID userId = UUID.randomUUID();
            repository.save(new Workspace("workspace-1", userId));
            Workspace workspaceToRemove = new Workspace("workspace-2", userId);
            repository.save(workspaceToRemove);
            repository.save(new Workspace("workspace-3", userId));

            repository.remove(workspaceToRemove);

            List<Workspace> workspaces = repository.getAll(userId);

            assertEquals(2, workspaces.size());
            assertEquals("workspace-1", workspaces.get(0).getName());
            assertEquals("workspace-3", workspaces.get(1).getName());
        }
    }
}