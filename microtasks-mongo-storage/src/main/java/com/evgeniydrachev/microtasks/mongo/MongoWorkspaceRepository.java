package com.evgeniydrachev.microtasks.mongo;

import com.evgeniydrachev.microtasks.core.workspace.Category;
import com.evgeniydrachev.microtasks.core.workspace.Task;
import com.evgeniydrachev.microtasks.core.workspace.Workspace;
import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import org.bson.Document;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import static com.evgeniydrachev.microtasks.core.workspace.Category.*;

public class MongoWorkspaceRepository implements WorkspaceRepository {
    private static final ReplaceOptions REPLACE_OPTIONS = new ReplaceOptions().upsert(true);

    private final MongoCollection<Document> collection;

    public MongoWorkspaceRepository(MongoClient client) {

        MongoDatabase database = client.getDatabase("microtasks");
        collection = database.getCollection("workspaces");
    }

    @Override
    public Workspace getOrCreateDefault(UUID userId) {
        Document document = collection.find(Filters.eq("userId", userId.toString())).first();
        if (document == null) {
            Workspace workspace = new Workspace(userId);
            collection.insertOne(toDocument(workspace));
            return workspace;
        }

        return toWorkspace(document);
    }

    @Override
    public void save(Workspace workspace) {
        Document document = toDocument(workspace);
        collection.replaceOne(new Document("_id", workspace.getId().toString()), document, REPLACE_OPTIONS);
    }

    @Override
    public Optional<Workspace> get(UUID uuid) {
        Document document = collection.find(Filters.eq("_id", uuid.toString())).first();
        if (document == null) {
            return Optional.empty();
        }
        return Optional.of(toWorkspace(document));
    }

    @Override
    public List<Workspace> getAll(UUID userId) {
        FindIterable<Document> documents = collection.find(Filters.eq("userId", userId.toString()));
        ArrayList<Workspace> workspaces = documents
                .map(MongoWorkspaceRepository::toWorkspace)
                .into(new ArrayList<>());
        return workspaces;
    }

    @Override
    public void remove(Workspace workspace) {
        collection.deleteOne(Filters.eq("_id", workspace.getId().toString()));
    }

    private static Workspace toWorkspace(Document document) {

        UUID id = UUID.fromString(document.getString("_id"));
        UUID userId = UUID.fromString(document.getString("userId"));
        String name = document.get("name", Workspace.DEFAULT_NAME);

        Workspace workspace = new Workspace(id, name, userId);

        for (Category category : List.of(TODAY, SOON, LATER)) {
            List<Document> documents = document.getList(category.name(), Document.class);
            ListIterator<Document> iterator = documents.listIterator(documents.size());
            while (iterator.hasPrevious()) {
                workspace.addTask(toTask(iterator.previous()), category);
            }
        }

        return workspace;

    }

    private static Task toTask(Document document) {
        return new Task(
                UUID.fromString(document.getString("uuid")),
                document.getString("summary"),
                document.getBoolean("completed"),
                toInstant(document.getDate("created"))
        );
    }

    private static Document toDocument(Workspace workspace) {
        Document document = new Document("_id", workspace.getId().toString())
                .append("userId", workspace.getUserId().toString())
                .append("name", workspace.getName());

        for (Category category : List.of(TODAY, SOON, LATER)) {
            List<Document> taskDocuments = new ArrayList<>();
            for (Task task : workspace.getTasks(category)) {
                taskDocuments.add(toDocument(task));
            }

            for (Task task : workspace.getCompletedTasks(category)) {
                taskDocuments.add(toDocument(task));
            }

            document.append(category.name(), taskDocuments);
        }

        return document;
    }

    private static Document toDocument(Task task) {
        return new Document("uuid", task.getUuid().toString())
                .append("summary", task.getSummary())
                .append("completed", task.isCompleted())
                .append("created", toDate(task.getCreated()));
    }

    private static Date toDate(Instant instant) {
        return instant == null
                ? null
                : Date.from(instant);
    }

    private static Instant toInstant(Date date) {
        return date == null
                ? null
                : date.toInstant();
    }
}
