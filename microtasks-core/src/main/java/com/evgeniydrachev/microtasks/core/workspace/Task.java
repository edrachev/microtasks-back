package com.evgeniydrachev.microtasks.core.workspace;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

public class Task implements Serializable {

    private final UUID uuid;
    private boolean completed;
    private String summary;
    private Instant created = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    public Task() {
        this.uuid = UUID.randomUUID();
    }

    public Task(String summary) {
        this.uuid = UUID.randomUUID();
        this.summary = summary;
    }

    public Task(UUID uuid, String summary) {
        this.uuid = uuid;
        this.summary = summary;
    }

    public Task(UUID uuid, String summary, boolean completed, Instant created) {
        this.uuid = uuid;
        this.summary = summary;
        this.completed = completed;
        this.created = created != null ? created.truncatedTo(ChronoUnit.MILLIS) : null;
    }

    public String getSummary() {
        return summary;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }
}
