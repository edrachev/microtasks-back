package com.evgeniydrachev.microtasks.core.workspace;

import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;

import java.util.UUID;

public class WorkspaceService {
    private final WorkspaceRepository repository;

    public WorkspaceService(WorkspaceRepository repository) {
        this.repository = repository;
    }

    public Workspace getUserWorkspace(UUID userId, UUID workspaceId) {
        Workspace workspace = repository.get(workspaceId)
                .orElseThrow(() -> new WorkspaceNotFoundException(workspaceId));
        if (!workspace.getUserId().equals(userId)) {
            throw new SecurityException("the user is not the owner of the workspace");
        }

        return workspace;
    }

    public void remove(UUID userId, UUID workspaceId) {
        Workspace workspace = getUserWorkspace(userId, workspaceId);
        if (!workspace.isEmpty()) {
            throw new IllegalStateException("workspace is not empty");
        }
        repository.remove(workspace);
    }
}
