package com.evgeniydrachev.microtasks.core.workspace;

import java.util.UUID;

public class WorkspaceNotFoundException extends RuntimeException {
    public WorkspaceNotFoundException(UUID uuid) {
        super(String.format("workspace for uuid=%s not found", uuid));
    }
}
