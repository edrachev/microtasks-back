package com.evgeniydrachev.microtasks.core.workspace;

import java.io.Serializable;
import java.util.*;

public class Workspace implements Serializable {
    public static final String DEFAULT_NAME = "Tasks";

    private final UUID id;
    private final UUID userId;

    private String name;

    private final Map<Category, TaskBox> boxes = new HashMap<>();

    public Workspace(UUID id, String name, UUID userId) {
        this.id = id;
        this.name = name;
        this.userId = userId;

        boxes.put(Category.TODAY, new TaskBox());
        boxes.put(Category.SOON, new TaskBox());
        boxes.put(Category.LATER, new TaskBox());
    }

    public Workspace(String name, UUID userId) {
        this(UUID.randomUUID(), name, userId);
    }

    public Workspace(UUID userId) {
        this(UUID.randomUUID(), DEFAULT_NAME, userId);
    }

    public void addTask(Task task, Category category) {
        boxes.get(category).insertFirst(task);
    }

    public void completeTask(UUID uuid) {
        Task task = foundTaskOrThrowException(uuid);
        task.setCompleted(true);
    }

    private Task foundTaskOrThrowException(UUID uuid) {
        Optional<Task> foundTask = findTask(uuid);
        if (!foundTask.isPresent()) {
            throw new IllegalArgumentException("task was not found");
        }
        return foundTask.get();
    }

    public void uncompleteTask(UUID uuid) {
        Task task = foundTaskOrThrowException(uuid);
        task.setCompleted(false);
    }

    public void removeTask(UUID uuid) {
        Optional<TaskBox> foundBox = findBoxWithTask(uuid);
        if (foundBox.isPresent()) {
            foundBox.get().remove(uuid);
        } else {
            System.out.println("task was not found for removing");
        }
    }

    public void updateTask(Task task, Category category, int order) {
        Optional<TaskBox> foundBox = findBoxWithTask(task.getUuid());
        if (!foundBox.isPresent()) {
            throw new IllegalArgumentException("task was not found");
        }
        // save original creation date
        Task oldTask = foundBox.get().get(task.getUuid()).orElseThrow();
        task.setCreated(oldTask.getCreated());

        foundBox.get().remove(task.getUuid());
        boxes.get(category).insert(order, task);

    }

    private Optional<Task> findTask(UUID uuid) {
        for (var box : boxes.values()) {
            Optional<Task> found = box.get(uuid);
            if (found.isPresent()) {
                return found;
            }
        }

        return Optional.empty();
    }

    private Optional<TaskBox> findBoxWithTask(UUID uuid) {
        for (TaskBox box : boxes.values()) {
            Optional<Task> found = box.get(uuid);
            if (found.isPresent()) {
                return Optional.of(box);
            }
        }

        return Optional.empty();
    }

    public List<Task> getTasks(Category category) {
        return boxes.get(category).getActual();
    }

    public List<Task> getCompletedTasks(Category category) {
        return boxes.get(category).getCompleted();
    }

    public UUID getUserId() {
        return userId;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEmpty() {
        return boxes.values().stream()
                .flatMap(box -> box.getActual().stream())
                .findAny().isEmpty();
    }
}
