package com.evgeniydrachev.microtasks.core.workspace.repository;

import com.evgeniydrachev.microtasks.core.workspace.Workspace;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryWorkspaceRepository implements WorkspaceRepository {
    Map<UUID, Workspace> map = new LinkedHashMap<>();

    @Override
    public Workspace getOrCreateDefault(UUID userId) {
        List<Workspace> workspaces = getAll(userId);
        Workspace workspace = workspaces.stream().findAny().orElse(null);
        if (workspace == null) {
            workspace = new Workspace(userId);
            map.put(workspace.getId(), workspace);
        }

        return workspace;
    }

    @Override
    public void save(Workspace workspace) {
        map.put(workspace.getId(), workspace);
    }

    @Override
    public Optional<Workspace> get(UUID uuid) {
        return Optional.ofNullable(map.get(uuid));
    }

    @Override
    public List<Workspace> getAll(UUID userId) {
        return map.values().stream()
                .filter(workspace -> workspace.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public void remove(Workspace workspace) {
        map.remove(workspace.getId());
    }
}
