package com.evgeniydrachev.microtasks;

import com.evgeniydrachev.microtasks.core.users.InMemoryUserRepository;
import com.evgeniydrachev.microtasks.core.users.UserRepository;
import com.evgeniydrachev.microtasks.core.workspace.repository.InMemoryWorkspaceRepository;
import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;
import com.evgeniydrachev.microtasks.security.KeyPairProvider;
import com.evgeniydrachev.microtasks.users.GoogleVerifierFacade;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureWebTestClient
class CorsTest {

    @TestConfiguration
    public static class TestConfig {
        @Bean
        public WorkspaceRepository workspaceRepository() {
            return new InMemoryWorkspaceRepository();
        }

        @Bean
        public UserRepository userRepository() {
            return new InMemoryUserRepository();
        }

        @Bean
        public KeyPairProvider keyPairProvider() {
            return new TestKeyPairProvider();
        }
    }

    @Autowired
    private WebTestClient webClient;

    @MockBean
    private GoogleVerifierFacade facade;

    @Autowired
    private KeyPairProvider keyPairProvider;

    @ParameterizedTest
    @ValueSource(strings = {
            "https://local.microtasks.app:3000",
            "http://local.microtasks.app:3000"
    })
    public void readTasks(String origin) {
        String jwt = Jwts.builder().signWith(keyPairProvider.get().getPrivate()).setSubject(UUID.randomUUID().toString()).compact();

        webClient.get()
                .uri("http://localhost:8080/api/v1/workspace")
                .header("Authorization", "Bearer " + jwt.toString())
                .header("Origin", origin)
                .exchange()
                .expectStatus().isOk()
                .expectHeader()
                .valueEquals("Access-Control-Allow-Origin", origin);
    }

    @Test
    public void readTasksFromBadOrigin() {
        String jwt = Jwts.builder().signWith(keyPairProvider.get().getPrivate()).setSubject(UUID.randomUUID().toString()).compact();

        webClient.get()
                .uri("http://localhost:8080/api/v1/workspace")
                .header("Authorization", "Bearer " + jwt.toString())
                .header("Origin", "http://badorigin.com")
                .exchange()
                .expectStatus().isForbidden();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "https://local.microtasks.app:3000",
            "http://local.microtasks.app:3000"
    })
    public void authorizeNewUser(String origin) {
        GoogleIdToken idToken = GoogleTokenUtils.generate("email");

        when(facade.getGoogleIdToken("valid_token")).thenReturn(Optional.of(idToken));

        webClient.post()
                .uri("http://localhost:8080/api/v1/users/authenticate")
                .header("Content-Type", "application/json")
                .header("Origin", origin)
                .bodyValue("""
                        {
                          "idToken": "valid_token"
                        }
                        """)
                .exchange()
                .expectStatus().isOk()
                .expectHeader()
                .valueEquals("Access-Control-Allow-Origin", origin)
                .expectBody()
                .jsonPath("$.access_token").isNotEmpty();
    }

    @Test
    public void authorizeNewUserFromBadOrigin() {
        GoogleIdToken idToken = GoogleTokenUtils.generate("email");

        when(facade.getGoogleIdToken("valid_token")).thenReturn(Optional.of(idToken));

        webClient.post()
                .uri("http://localhost:8080/api/v1/users/authenticate")
                .header("Content-Type", "application/json")
                .header("Origin", "http://badorigin.com")
                .bodyValue("""
                        {
                          "idToken": "valid_token"
                        }
                        """)
                .exchange()
                .expectStatus().isForbidden();
    }
}