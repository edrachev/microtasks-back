package com.evgeniydrachev.microtasks;

import com.evgeniydrachev.microtasks.core.workspace.Category;
import com.evgeniydrachev.microtasks.core.workspace.Task;
import com.evgeniydrachev.microtasks.core.workspace.Workspace;
import com.evgeniydrachev.microtasks.core.workspace.repository.InMemoryWorkspaceRepository;
import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;
import com.evgeniydrachev.microtasks.security.KeyPairProvider;
import com.fasterxml.jackson.databind.JsonNode;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.security.KeyPair;
import java.time.Instant;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.UUID.randomUUID;

@SpringBootTest
@AutoConfigureWebTestClient
class WorkspacesResourceTest {

    @TestConfiguration
    public static class TestConfig {
        @Bean
        public WorkspaceRepository workspaceRepository() {
            return new InMemoryWorkspaceRepository();
        }

        @Bean
        public KeyPairProvider keyPairProvider() {
            return new TestKeyPairProvider();
        }
    }

    @Autowired
    private WebTestClient webClient;

    @Autowired
    private WorkspaceRepository repository;

    @Autowired
    private KeyPairProvider keyPairProvider;

    private String authorizationHeader;
    private UUID userId;
    private UUID workspaceId;

    private UUID strangerWorkspaceId;

    @BeforeEach
    public void beforeEach() {
        userId = UUID.randomUUID();
        String jwt = Jwts.builder().signWith(keyPairProvider.get().getPrivate()).setSubject(userId.toString()).compact();
        authorizationHeader = "Bearer " + jwt;

        Workspace workspace = new Workspace(userId);
        workspaceId = workspace.getId();
        workspace.addTask(new Task("Improve yourself"), Category.TODAY);
        workspace.addTask(new Task(UUID.randomUUID(), "Improve others", false, null), Category.SOON);
        workspace.addTask(new Task(UUID.randomUUID(), "Improve the world", false, Instant.ofEpochMilli(0)), Category.LATER);
        repository.save(workspace);

        Workspace strangerWorkspace = new Workspace(UUID.randomUUID());
        strangerWorkspaceId = strangerWorkspace.getId();
        repository.save(strangerWorkspace);
    }

    @Test
    public void unathorizedAddTask() {
        webClient.post()
                .uri("/api/v1/workspaces/{uuid}/create-task", workspaceId)
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "summary": "new task",
                          "category": "SOON"
                        }
                        """)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    public void unathorizedGetTasks() {
        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    public void wrongTokenSignatureGetTasks() {
        KeyPair keyPair = Keys.keyPairFor(SignatureAlgorithm.RS256);

        UUID userId = UUID.randomUUID();
        String jwt = Jwts.builder().signWith(keyPair.getPrivate()).setSubject(userId.toString()).compact();

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", "Bearer " + jwt)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    public void unathorizedUpdateTask() {
        webClient.post()
                .uri("/api/v1/workspaces/{uuid}/update-task", workspaceId)
                .header("Content-Type", "application/json")
                .bodyValue(format("""
                        {
                          "task": {
                            "uuid": "%s",
                            "summary": "updated task"
                          },
                          "category": "LATER",
                          "order": 1
                        }
                        """, randomUUID()))
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    public void readStrangerTasks() {
        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", strangerWorkspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    public void readTasks() {
        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(3)
                .jsonPath("$[0].category").isEqualTo("TODAY")
                .jsonPath("$[0].tasks.length()").isEqualTo(1)
                .jsonPath("$[0].tasks[0].uuid").isNotEmpty()
                .jsonPath("$[0].tasks[0].summary").isEqualTo("Improve yourself")
                .jsonPath("$[0].tasks[0].created").isNotEmpty()
                .jsonPath("$[1].category").isEqualTo("SOON")
                .jsonPath("$[1].tasks.length()").isEqualTo(1)
                .jsonPath("$[1].tasks[0].uuid").isNotEmpty()
                .jsonPath("$[1].tasks[0].summary").isEqualTo("Improve others")
                .jsonPath("$[1].tasks[0].created").isEmpty()
                .jsonPath("$[2].category").isEqualTo("LATER")
                .jsonPath("$[2].tasks.length()").isEqualTo(1)
                .jsonPath("$[2].tasks[0].uuid").isNotEmpty()
                .jsonPath("$[2].tasks[0].summary").isEqualTo("Improve the world")
                .jsonPath("$[2].tasks[0].created").isEqualTo("1970-01-01T00:00:00Z");
    }

    @Test
    public void addTask() {
        webClient.post()
                .uri("/api/v1/workspaces/{uuid}/create-task", workspaceId)
                .header("Authorization", authorizationHeader)
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "summary": "new task",
                          "category": "SOON"
                        }
                        """)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.uuid").isNotEmpty();

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[1].tasks[0].summary").isEqualTo("new task")
                .jsonPath("$[1].tasks[0].created").isNotEmpty();
    }

    @Test
    public void updateTask() {
        FluxExchangeResult<JsonNode> resultFlux = webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .returnResult(JsonNode.class);

        JsonNode jsonNode = resultFlux.getResponseBody().blockFirst();
        String uuid = jsonNode.get("tasks").get(0).get("uuid").asText();
        String created = jsonNode.get("tasks").get(0).get("created").asText();

        webClient.post()
                .uri("/api/v1/workspaces/{uuid}/update-task", workspaceId)
                .header("Authorization", authorizationHeader)
                .header("Content-Type", "application/json")
                .bodyValue(format("""
                        {
                          "uuid": "%s",
                          "summary": "updated task",
                          "category": "LATER",
                          "order": 1
                        }
                        """, uuid))
                .exchange()
                .expectStatus().isOk();

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].category").isEqualTo("TODAY")
                .jsonPath("$[0].tasks.length()").isEqualTo(0)
                .jsonPath("$[2].category").isEqualTo("LATER")
                .jsonPath("$[2].tasks.length()").isEqualTo(2)
                .jsonPath("$[2].tasks[0].uuid").isNotEmpty()
                .jsonPath("$[2].tasks[0].summary").isEqualTo("Improve the world")
                .jsonPath("$[2].tasks[0].created").isNotEmpty()
                .jsonPath("$[2].tasks[1].uuid").isEqualTo(uuid)
                .jsonPath("$[2].tasks[1].summary").isEqualTo("updated task")
                .jsonPath("$[2].tasks[1].created").isEqualTo(created);
    }

    @Test
    public void completeAndUncompleteTask() {
        FluxExchangeResult<JsonNode> resultFlux = webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .returnResult(JsonNode.class);

        JsonNode jsonNode = resultFlux.getResponseBody().blockLast();
        String uuid = jsonNode.get("tasks").get(0).get("uuid").asText();

        webClient.post()
                .uri("/api/v1/workspaces/{uuid}/complete-task", workspaceId)
                .header("Authorization", authorizationHeader)
                .header("Content-Type", "application/json")
                .bodyValue(format("""
                        {
                          "uuid": "%s"
                        }
                        """, uuid))
                .exchange()
                .expectStatus().isOk();

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].category").isEqualTo("TODAY")
                .jsonPath("$[0].tasks.length()").isEqualTo(1)
                .jsonPath("$[1].category").isEqualTo("SOON")
                .jsonPath("$[1].tasks.length()").isEqualTo(1)
                .jsonPath("$[2].category").isEqualTo("LATER")
                .jsonPath("$[2].tasks.length()").isEqualTo(0);

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}/completed-tasks", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(1)
                .jsonPath("$[0].uuid").isEqualTo(uuid);

        webClient.post()
                .uri("/api/v1/workspaces/{uuid}/uncomplete-task", workspaceId)
                .header("Authorization", authorizationHeader)
                .header("Content-Type", "application/json")
                .bodyValue(format("""
                        {
                          "uuid": "%s"
                        }
                        """, uuid))
                .exchange()
                .expectStatus().isOk();

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].category").isEqualTo("TODAY")
                .jsonPath("$[0].tasks.length()").isEqualTo(1)
                .jsonPath("$[1].category").isEqualTo("SOON")
                .jsonPath("$[1].tasks.length()").isEqualTo(1)
                .jsonPath("$[2].category").isEqualTo("LATER")
                .jsonPath("$[2].tasks.length()").isEqualTo(1);

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}/completed-tasks", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(0);
    }

    @Test
    public void removeTask() {
        FluxExchangeResult<JsonNode> resultFlux = webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .returnResult(JsonNode.class);

        JsonNode jsonNode = resultFlux.getResponseBody().blockFirst();
        String uuid = jsonNode.get("tasks").get(0).get("uuid").asText();

        webClient.delete()
                .uri("/api/v1/workspaces/{workspaceId}/task/{uuid}", workspaceId, uuid)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isNoContent();

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].category").isEqualTo("TODAY")
                .jsonPath("$[0].tasks.length()").isEqualTo(0)
                .jsonPath("$[1].category").isEqualTo("SOON")
                .jsonPath("$[1].tasks.length()").isEqualTo(1)
                .jsonPath("$[2].category").isEqualTo("LATER")
                .jsonPath("$[2].tasks.length()").isEqualTo(1);
    }

    @Test
    public void workspaceNotFound() {

        String uuid = UUID.randomUUID().toString();

        webClient.put()
                .uri("/api/v1/workspaces/{uuid}", uuid)
                .header("Authorization", authorizationHeader)
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "name": "updated workspace"
                        }
                        """)
                .exchange()
                .expectStatus().isNotFound();

        webClient.delete()
                .uri("/api/v1/workspaces/{uuid}", uuid)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isNotFound();

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", uuid)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    public void testWorkspacesCRUD() {
        webClient.get()
                .uri("/api/v1/workspaces")
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(1);

        FluxExchangeResult<JsonNode> resultFlux = webClient.post()
                .uri("/api/v1/workspaces")
                .header("Authorization", authorizationHeader)
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "name": "new workspace"
                        }
                        """)
                .exchange()
                .expectStatus().isOk()
                .returnResult(JsonNode.class);

        JsonNode jsonNode = resultFlux.getResponseBody().blockLast();
        String uuid = jsonNode.get("uuid").asText();

        webClient.get()
                .uri("/api/v1/workspaces")
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(2)
                .jsonPath("$[0].name").isEqualTo("Tasks")
                .jsonPath("$[1].uuid").isEqualTo(uuid)
                .jsonPath("$[1].name").isEqualTo("new workspace");

        webClient.put()
                .uri("/api/v1/workspaces/{uuid}", uuid)
                .header("Authorization", authorizationHeader)
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "name": "updated workspace"
                        }
                        """)
                .exchange()
                .expectStatus().isOk();

        webClient.get()
                .uri("/api/v1/workspaces")
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(2)
                .jsonPath("$[0].name").isEqualTo("Tasks")
                .jsonPath("$[1].uuid").isEqualTo(uuid)
                .jsonPath("$[1].name").isEqualTo("updated workspace");

        webClient.delete()
                .uri("/api/v1/workspaces/{uuid}", uuid)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isNoContent();

        webClient.get()
                .uri("/api/v1/workspaces")
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(1)
                .jsonPath("$[0].name").isEqualTo("Tasks");
    }

    @Test
    public void removeNotEmptyWorkspace() {
        webClient.delete()
                .uri("/api/v1/workspaces/{uuid}", workspaceId)
                .header("Authorization", authorizationHeader)
                .exchange()
                .expectStatus().is4xxClientError();
    }
}