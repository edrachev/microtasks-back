package com.evgeniydrachev.microtasks;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;

import java.time.Instant;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GoogleTokenUtils {

    public static GoogleIdToken generate(String email) {
        return generate(email, true, false);
    }

    public static GoogleIdToken generate(String email, boolean isEmailVerified, boolean isExpired) {
        GoogleIdToken idToken = mock(GoogleIdToken.class);
        GoogleIdToken.Payload payload = new GoogleIdToken.Payload();
        when(idToken.getPayload()).thenReturn(payload);

        payload.setEmail(email);
        payload.setEmailVerified(isEmailVerified);

        long nowSeconds = Instant.now().getEpochSecond();
        Long expirationTime = isExpired ? nowSeconds - 1 : nowSeconds + 1000;
        payload.setExpirationTimeSeconds(expirationTime);

        return idToken;
    }
}
