package com.evgeniydrachev.microtasks.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.server.resource.BearerTokenAuthenticationToken;
import org.springframework.security.oauth2.server.resource.InvalidBearerTokenException;
import reactor.core.publisher.Mono;

public class JwtTokenAuthenticationManager implements ReactiveAuthenticationManager {

    private final JwtParser parser;

    public JwtTokenAuthenticationManager(KeyPairProvider keyPairProvider) {
        parser = Jwts.parserBuilder()
                .setSigningKey(keyPairProvider.get().getPublic())
                .build();
    }

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        BearerTokenAuthenticationToken token = (BearerTokenAuthenticationToken) authentication;

        try {
            Jws<Claims> claimsJws = parser.parseClaimsJws(token.getToken());
            return Mono.just(new JwtWithUserIdAuthentication(claimsJws.getBody().getSubject()));
        } catch (SignatureException e) {
            return Mono.error(new InvalidBearerTokenException("signature is not valid"));
        }
    }
}
