package com.evgeniydrachev.microtasks.workspace;

import com.evgeniydrachev.microtasks.core.workspace.Task;
import com.evgeniydrachev.microtasks.core.workspace.Workspace;
import com.evgeniydrachev.microtasks.core.workspace.WorkspaceNotFoundException;
import com.evgeniydrachev.microtasks.core.workspace.WorkspaceService;
import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;
import com.evgeniydrachev.microtasks.security.JwtWithUserIdAuthentication;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.evgeniydrachev.microtasks.core.workspace.Category.*;

@RestController
@RequestMapping("/api/v1/workspaces")
public class WorkspacesResource {

    private final WorkspaceService service;
    private final WorkspaceRepository repository;

    public WorkspacesResource(WorkspaceService service, WorkspaceRepository repository) {
        this.service = service;
        this.repository = repository;
    }

    @GetMapping
    public List<WorkspaceResponse> get(JwtWithUserIdAuthentication authentication) {
        List<Workspace> workspaces = repository.getAll(authentication.getUserId());
        return workspaces.stream().map(WorkspaceResponse::new).collect(Collectors.toList());
    }

    @PostMapping
    public UuidResponse create(@RequestBody WorkspaceRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = new Workspace(request.getName(), authentication.getUserId());
        repository.save(workspace);
        return new UuidResponse(workspace.getId());
    }

    @PutMapping("/{uuid}")
    public void update(@PathVariable UUID uuid, @RequestBody WorkspaceRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = service.getUserWorkspace(authentication.getUserId(), uuid);
        workspace.setName(request.getName());
        repository.save(workspace);
    }

    @DeleteMapping("/{uuid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable UUID uuid, JwtWithUserIdAuthentication authentication) {
        service.remove(authentication.getUserId(), uuid);
    }

    @GetMapping("/{uuid}")
    public List<TaskBox> getById(@PathVariable UUID uuid, JwtWithUserIdAuthentication authentication) {

        Workspace workspace = service.getUserWorkspace(authentication.getUserId(), uuid);
        List<TaskBox> boxes = new ArrayList<>();
        boxes.add(new TaskBox(TODAY, mapToResponse(workspace.getTasks(TODAY))));
        boxes.add(new TaskBox(SOON, mapToResponse(workspace.getTasks(SOON))));
        boxes.add(new TaskBox(LATER, mapToResponse(workspace.getTasks(LATER))));
        return boxes;
    }

    @PostMapping("/{uuid}/update-task")
    @Operation(summary = "Update the task, changing rank will affect other tasks rank inside a category")
    public void updateTask(@PathVariable UUID uuid, @RequestBody UpdateTaskRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = service.getUserWorkspace(authentication.getUserId(), uuid);
        workspace.updateTask(request.getTask(), request.getCategory(), request.getOrder());
        repository.save(workspace);
    }

    @PostMapping("/{uuid}/create-task")
    @Operation(summary = "Create new task, the task will be the first in the category")
    public UuidResponse createTask(@PathVariable UUID uuid, @RequestBody AddTaskRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = service.getUserWorkspace(authentication.getUserId(), uuid);
        Task task = new Task(request.getSummary());
        workspace.addTask(task, request.getCategory());
        repository.save(workspace);
        return new UuidResponse(task.getUuid());
    }

    @PostMapping("/{uuid}/complete-task")
    @Operation(summary = "Complete the task with passed id, the task will no longer be returned")
    public void completeTask(@PathVariable UUID uuid, @RequestBody UuidRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = service.getUserWorkspace(authentication.getUserId(), uuid);
        workspace.completeTask(request.getUuid());
        repository.save(workspace);
    }

    @GetMapping("/{uuid}/completed-tasks")
    @Operation(summary = "List of completed tasks")
    public List<TaskResponse> completedTasks(@PathVariable UUID uuid, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = service.getUserWorkspace(authentication.getUserId(), uuid);
        List<TaskResponse> tasks = new ArrayList<>();
        tasks.addAll(mapToResponse(workspace.getCompletedTasks(TODAY)));
        tasks.addAll(mapToResponse(workspace.getCompletedTasks(SOON)));
        tasks.addAll(mapToResponse(workspace.getCompletedTasks(LATER)));
        return tasks;
    }

    private List<TaskResponse> mapToResponse(List<Task> tasks) {
        return tasks.stream().map(TaskResponse::new).collect(Collectors.toList());
    }

    @PostMapping("/{uuid}/uncomplete-task")
    @Operation(summary = "Uncomplete the task with passed id, the task will be returned to workspace")
    public void uncompleteTask(@PathVariable UUID uuid, @RequestBody UuidRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = service.getUserWorkspace(authentication.getUserId(), uuid);
        workspace.uncompleteTask(request.getUuid());
        repository.save(workspace);
    }

    @DeleteMapping("/{uuid}/task/{taskUuid}")
    @Operation(summary = "Remove the task with passed id, the task will no longer be returned")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTask(@PathVariable UUID uuid, @PathVariable UUID taskUuid, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = service.getUserWorkspace(authentication.getUserId(), uuid);
        workspace.removeTask(taskUuid);
        repository.save(workspace);
    }

    @ExceptionHandler(value = {IllegalStateException.class})
    public ResponseEntity<Object> exceptionHandler() {
        return ResponseEntity.status(400).build();
    }

    @ExceptionHandler(value = {WorkspaceNotFoundException.class})
    public ResponseEntity<Object> workspaceNotFoundExceptionHandler() {
        return ResponseEntity.status(404).build();
    }
}
