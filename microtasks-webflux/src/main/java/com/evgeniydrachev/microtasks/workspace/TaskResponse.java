package com.evgeniydrachev.microtasks.workspace;

import com.evgeniydrachev.microtasks.core.workspace.Task;

import java.time.Instant;
import java.util.UUID;

public class TaskResponse {

    private final Task task;

    public TaskResponse(Task task) {
        this.task = task;
    }

    public UUID getUuid() {
        return task.getUuid();
    }

    public String getSummary() {
        return task.getSummary();
    }

    public Instant getCreated() {
        return task.getCreated();
    }
}
