package com.evgeniydrachev.microtasks.workspace;

import java.util.UUID;

public class UuidResponse {
    private final UUID uuid;

    public UuidResponse(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }
}
